import static org.junit.jupiter.api.Assertions.*;

class StoreTest {

    @org.junit.jupiter.api.Test
    void checkout() {
        Store store = new Store();

        assertEquals(store.checkout(), 0, "Given no input, the cost should be 0");
    }

}