# DOJO

This is a Code shell for a DOJO

## Prerequisites

### Needed
- JDK installed

### Recommended
- A modern IDE such as IntelliJ

# Getting started
This shell includes a class for the store with one checkout method 
for your convenience. Either run the tests in StoreTest if you want 
to work with tests / TDD, or you can run the main file.
